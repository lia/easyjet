#ifndef LLTTANALYSIS_CHANNELS
#define LLTTANALYSIS_CHANNELS

namespace HLLTT
{

  enum Channel
  {
    LepLep = 0,
    LepHad = 1,
    HadHad = 2,
  };

}

#endif
